package com.aprilbrain.jubal.testbusevent;

/**
 * Created by Jubal on 3/1/16.
 * Used in EventBus
 */
public class DuduEvent {
    public final String message;

    public DuduEvent(String message) {
        this.message = message;
    }
}
